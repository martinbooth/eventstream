function Run-CS-Tests {
    param(
        [string]$BaseDir,
        [string[]]$TestProjectsDir,
        [string]$TestProjectsBaseDir,
        [string]$BuildConfiguration,
        [string]$TestCategory,
        [string]$TestSettingsFileName,
        [string]$TestRunConfigFileName,
        # [bool]$EnableCodeMetrics,
        # [string]$TestCoverageExclusion,
        [string]$BuildPlatform,
        [string]$VisualStudioVersion
    )

    if (!$VisualStudioVersion) {
        $VisualStudioVersion = Get-Highest-Installed-Version-Of-Visual-Studio
    }

    $msTestPath = Get-MsTest-Path $VisualStudioVersion

    Create-Output-Directories

    $categoryArg = if ($TestCategory) {
        " /category:$TestCategory"
    } else { '' }

    $testSettingsArg = if ($TestSettingsFileName) {
        " /testsettings:`"$BaseDir\$TestSettingsFileName`""
    } else { '' }

    $testRunConfigArg = if ($TestRunConfigFileName) {
        " /runconfig:`"$BaseDir\$TestRunConfigFileName`""
    } else { '' }

    foreach ($testProjectDir in $TestProjectsDir) {
        Write-Verbose "Running tests for project $testProjectDir"

        foreach ($testDllFileName in Get-ChildItem -Path "$TestProjectsBaseDir\$testProjectDir\bin\$BuildConfiguration" -Recurse '*Tests.dll') {
            Write-Verbose "Running tests for DLL $testDllFileName"

            $trxFilePath = 'Output\TestResults\{0}.trx' -F $(Nicify-TestProjectDir $testProjectDir)
      
            if(Test-Path $trxFilePath){
                Remove-Item $trxFilePath
            }
      
            #if ($EnableCodeMetrics -eq $true) {
            #    Write-Verbose "--- Running tests with coverage on $file ---"
            #    Run-CS-Tests-With-Coverage $testProjectDir $trxFilePath $msTestPath $(($testDllFileName).FullName) $TestCoverageExclusion $categoryArg $testSettingsArg $testRunConfigArg $VisualStudioVersion
            #} else {
            #    Write-Verbose "--- Running tests without coverage on $file ---"
                Run-CS-Tests-Without-Coverage $testProjectDir $trxFilePath $msTestPath $(($testDllFileName).FullName) $categoryArg $testSettingsArg $testRunConfigArg $VisualStudioVersion
            #}
        }
    } 
}

function Create-Output-Directories {

    if(!(Test-Path -Path "Output" )){
        New-Item -ItemType directory -Path "Output" | Out-Null
    }

    if(!(Test-Path -Path 'Output\TestResults' )){
        New-Item -ItemType directory -Path 'Output\TestResults' | Out-Null
    }
    else {
        Remove-Item 'Output\TestResults\*' -Recurse | Out-Null
    }
    
    if(!(Test-Path -Path 'Output\Coverage' )){
        New-Item -ItemType directory -Path 'Output\Coverage' | Out-Null
    }

    if(!(Test-Path -Path 'Output\NugetPackages' )){
        New-Item -ItemType directory -Path 'Output\NugetPackages' | Out-Null
    }
}

function Get-Visual-Studio-Installation-Path {
    param(
        [string]$VisualStudioVersion
    )

    $registryKeyTemplate = 'HKLM:\Software\Microsoft\VisualStudio\{0}'
    
    if ([System.IntPtr]::Size -ne 4) {
        $registryKeyTemplate = 'HKLM:\Software\Wow6432Node\Microsoft\VisualStudio\{0}'
    }
    
    Write-Debug "registryKeyTemplate:$registryKeyTemplate"
    
    $registryKey = $registryKeyTemplate -F $VisualStudioVersion
    $installationPath = (Get-ItemProperty $registryKey).InstallDir
    
    Write-Debug "registryKey:$registryKey"
    Write-Debug "installationPath:$installationPath"
    
    if (!(Test-Path -Path $installationPath)){
        throw ("VS $VisualStudioVersion is not installed on your system. It is needed to run MsTest.")
    }
    
    return $installationPath
}

function Get-MsTest-Path {
    param(
        [string]$VisualStudioVersion
    )
    
    $installationPath = Get-Visual-Studio-Installation-Path $VisualStudioVersion
    $msTestPath = join-path -path $installationPath -childpath 'MsTest.exe'
    
    Write-Debug "msTestPath:$msTestPath"
  
    if (!(Test-Path $msTestPath)){
        throw ('VS ' + $VisualStudioVersion + ' is not installed on your system. It is needed to run MsTest.')
    }
  
    return $msTestPath
}

function Run-CS-Tests-Without-Coverage {
    param(
        [string]$TestProjectDir,
        [string]$TrxFilePath,
        [string]$MsTestPath,
        [string]$TestDllFileName,
        [string]$CategoryArg,
        [string]$TestSettingsArg,
        [string]$TestRunConfigArg,
        [string]$VisualStudioVersion
    )

    $args = @("/testcontainer:`"$TestDllFileName`" $CategoryArg $TestSettingsArg $TestRunConfigArg /resultsfile:`"$TrxFilePath`"")
    
    Write-Debug "Calling MsTest with args: $args"

    $result = (Start-Process -PassThru -FilePath "$MsTestPath" -Wait -NoNewWindow -ArgumentList $args)

    if ($result.ExitCode -ne 0) {
        
		Write-Error "One or more tests failed"

        throw ("Error executing tests for: $TestProjectDir ")
    }
}

function Nicify-TestProjectDir {
    param(
        [string]$TestProjectDir
    )
    
    $output = $TestProjectDir.replace('\', '.')
    
    return $output
}

function Nuget-Package {
    param(
        [string[]]$projects,
        [string] $configuration
    )

    $nugetPath = Get-ChildItem -recurse Nuget.exe -Path .\packages\NuGet.CommandLine.*\*
    
    foreach ($project in $projects) {
        Write-Verbose "Creating nuget package for $project"
        Write $project

        & $nugetPath pack $project -outputdirectory "Output\NugetPackages" -Prop Configuration=$configuration
    }
}
