﻿namespace EventStream.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Newtonsoft.Json.Linq;

    public static class JsonAssert
    {
        public static void AreEqual(string expected, string actual)
        {
            var actualJson = JToken.Parse(actual);
            var expectedJson = JToken.Parse(expected);

            if (JToken.DeepEquals(actualJson, expectedJson))
                return;

            throw new AssertFailedException(string.Format("Expected: '{0}' does not equal actual: '{1}'", expected, actual));
        }
    }
}
