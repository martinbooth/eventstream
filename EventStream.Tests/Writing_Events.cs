﻿namespace EventStream.Tests
{
    using System;
    using System.IO.Abstractions.TestingHelpers;

    using Fakes;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class Writing_Events
    {
        protected MockFileSystem MockFileSystem { get; private set; }

        protected FakeGuidGenerator GuidGenerator { get; private set; }

        protected FakeTimeProvider TimeProvider { get; private set; }

        [TestInitialize]
        public void BeforeEach()
        {
            var testContainerFactory = new TestContainerFactory();
            ContainerFactory.CurrentContainer = testContainerFactory.GetContainer();
            this.MockFileSystem = testContainerFactory.MockFileSystem;
            this.GuidGenerator = testContainerFactory.GuidGenerator;
            this.TimeProvider = testContainerFactory.TimeProvider;
        }

        protected EventStream CreateEventStream()
        {
            return EventStream.Create("C:\\stream", this.EventStreamConfiguration);
        }

        private Configuration EventStreamConfiguration
        {
            get { return new Configuration { PageCount = 2 }; }
        }

        [TestClass]
        public class When_Writing_First_Event : Writing_Events
        {
            [TestMethod]
            public void Creates_Head_Page()
            {
                var eventStream = this.CreateEventStream();
                eventStream.RaiseEvent("event");

                Assert.IsTrue(this.MockFileSystem.FileExists("C:\\stream\\head.feed"));
            }

            [TestMethod]
            public void Writes_Event_To_Head_Page()
            {
                var eventStream = this.CreateEventStream();

                var expected = @"Previous: 

Id: 68192720-ae8a-42c2-bc96-895b8cb18666
Type: event type
Timestamp: 2014-01-16T17:30:35.0000000
Content-Length: 12

""event body""

";

                this.GuidGenerator.SetNextGuids(new Guid("68192720-ae8a-42c2-bc96-895b8cb18666"));
                this.TimeProvider.SetNextTimes(new DateTime(2014, 1, 16, 17, 30, 35));

                eventStream.RaiseEvent("event type", "event body");

                var file = this.MockFileSystem.File.ReadAllText("C:\\stream\\head.feed");

                Assert.AreEqual(expected, file);
            }
        }

        [TestClass]
        public class When_Writing_Multiple_Events : Writing_Events
        {
            [TestMethod]
            public void Appends_Event_To_Head_Page()
            {
                var eventStream = this.CreateEventStream();

                var expected = @"Previous: 

Id: 68192720-ae8a-42c2-bc96-895b8cb18666
Type: event1
Timestamp: 2014-01-16T17:30:35.0000000
Content-Length: 12

""event body""

Id: 9239df37-6214-49a9-892d-edc6687336dd
Type: event2
Timestamp: 2015-02-17T18:45:10.0000000

";

                this.GuidGenerator.SetNextGuids(new Guid("68192720-ae8a-42c2-bc96-895b8cb18666"), new Guid("9239df37-6214-49a9-892d-edc6687336dd"));

                this.TimeProvider.SetNextTimes(new DateTime(2014, 1, 16, 17, 30, 35), new DateTime(2015, 2, 17, 18, 45, 10));

                eventStream.RaiseEvent("event1", "event body");
                eventStream.RaiseEvent("event2");

                var file = this.MockFileSystem.File.ReadAllText("C:\\stream\\head.feed");

                Assert.AreEqual(expected, file);
            }

            [TestMethod]
            public void Archives_Head_Page_If_Number_Of_Events_Exceed_Page_Count()
            {
                var eventStream = this.CreateEventStream();

                var expectedArchivePage = @"Previous: 

Id: 68192720-ae8a-42c2-bc96-895b8cb18666
Type: event1
Timestamp: 2014-01-16T17:30:35.0000000

Id: 9239df37-6214-49a9-892d-edc6687336dd
Type: event2
Timestamp: 2015-02-17T18:45:10.0000000

";

                var expectedHeadPage = @"Previous: archive/2c1a0676-f760-4af9-aaa0-f83e0cf4fec7.feed

Id: 91be25e2-a68c-4af8-9dec-d752179a56a9
Type: event3
Timestamp: 2016-03-18T19:10:22.0000000

";

                this.GuidGenerator.SetNextGuids(
                    // these guids become event ids
                    new Guid("68192720-ae8a-42c2-bc96-895b8cb18666"),
                    new Guid("9239df37-6214-49a9-892d-edc6687336dd"),
                    // this guid will be used for the archive filename
                    new Guid("2c1a0676-f760-4af9-aaa0-f83e0cf4fec7"),
                    // this guids become event ids
                    new Guid("91be25e2-a68c-4af8-9dec-d752179a56a9"));

                this.TimeProvider.SetNextTimes(
                    new DateTime(2014, 1, 16, 17, 30, 35),
                    new DateTime(2015, 2, 17, 18, 45, 10),
                    new DateTime(2016, 3, 18, 19, 10, 22));

                eventStream.RaiseEvent("event1");
                eventStream.RaiseEvent("event2");
                eventStream.RaiseEvent("event3");

                var actualHeadFile = this.MockFileSystem.File.ReadAllText("C:\\stream\\head.feed");

                Assert.AreEqual(expectedHeadPage, actualHeadFile);

                var actualArchivePageFile = this.MockFileSystem.File.ReadAllText("C:\\stream\\archive\\2c1a0676-f760-4af9-aaa0-f83e0cf4fec7.feed");

                Assert.AreEqual(expectedArchivePage, actualArchivePageFile);
            }
        }

        [TestClass]
        public class When_Event_Stream_Already_Exists : Writing_Events
        {
            [TestMethod]
            public void Event_Stream_Writing_Resumes_Where_It_Left_Off()
            {
                this.MockFileSystem.File.WriteAllText(@"C:\stream\head.feed", @"Previous: archive/2c1a0676-f760-4af9-aaa0-f83e0cf4fec7.feed

Id: 91be25e2-a68c-4af8-9dec-d752179a56a9
Type: event1
Timestamp: 2016-03-18T19:10:22.0000000

");

                var eventStream = this.CreateEventStream();

                const string expected = @"Previous: archive/2c1a0676-f760-4af9-aaa0-f83e0cf4fec7.feed

Id: 91be25e2-a68c-4af8-9dec-d752179a56a9
Type: event1
Timestamp: 2016-03-18T19:10:22.0000000

Id: 9239df37-6214-49a9-892d-edc6687336dd
Type: event2
Timestamp: 2015-02-17T18:45:10.0000000

";

                this.GuidGenerator.SetNextGuids(new Guid("9239df37-6214-49a9-892d-edc6687336dd"));
                this.TimeProvider.SetNextTimes(new DateTime(2015, 2, 17, 18, 45, 10));

                eventStream.RaiseEvent("event2");

                var actual = this.MockFileSystem.File.ReadAllText("C:\\stream\\head.feed");

                Assert.AreEqual(expected, actual);
            }

            [TestMethod]
            public void Event_Stream_Repairs_File_If_Incomplete()
            {
                this.MockFileSystem.File.WriteAllText(@"C:\stream\head.feed", @"Previous: archive/2c1a0676-f760-4af9-aaa0-f83e0cf4fec7.feed

Id: 91be25e2-a68c-4af8-9dec-d752179a56a9
Type: event1
Timestamp: 2016-03-18T19:10:22.0000000

Id: 9");

                var expected = @"Previous: archive/2c1a0676-f760-4af9-aaa0-f83e0cf4fec7.feed

Id: 91be25e2-a68c-4af8-9dec-d752179a56a9
Type: event1
Timestamp: 2016-03-18T19:10:22.0000000

";

                this.CreateEventStream();

                Assert.AreEqual(expected, this.MockFileSystem.File.ReadAllText(@"C:\stream\head.feed"));
            }
        }
    }
}
