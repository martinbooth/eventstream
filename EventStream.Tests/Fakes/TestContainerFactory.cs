﻿using System.IO;

namespace EventStream.Tests.Fakes
{
    using System.IO.Abstractions;
    using System.IO.Abstractions.TestingHelpers;

    using Autofac;

    internal class TestContainerFactory : ContainerFactory
    {
        public TestContainerFactory()
        {
            this.MockFileSystem = new MockFileSystem();
            this.GuidGenerator = new FakeGuidGenerator();
            this.TimeProvider = new FakeTimeProvider();
        }

        public FakeTimeProvider TimeProvider { get; set; }

        public FakeGuidGenerator GuidGenerator { get; private set; }

        public MockFileSystem MockFileSystem { get; private set; }

        protected override void RegisterServices(ContainerBuilder builder)
        {
            base.RegisterServices(builder);
            builder.RegisterInstance(this.MockFileSystem).As<IFileSystem>();
            builder.RegisterInstance(this.GuidGenerator).As<IGuidGenerator>();
            builder.RegisterInstance(this.TimeProvider).As<ITimeProvider>();
            builder.RegisterType<FakeFileStreamFactory>().As<IFileStreamFactory>();
        }
    }
}