﻿namespace EventStream.Tests.Fakes
{
    using System;
    using System.Collections.Generic;

    public class FakeTimeProvider : ITimeProvider
    {
        private readonly Queue<DateTime> nextTimes = new Queue<DateTime>();

        public void SetNextTimes(params DateTime[] nextTimes)
        {
            foreach (var time in nextTimes)
                this.nextTimes.Enqueue(time);
        }

        public DateTime Now
        {
            get
            {
                if (this.nextTimes.Count == 0)
                    return DateTime.Now;

                return this.nextTimes.Dequeue();
            }
        }
    }
}
