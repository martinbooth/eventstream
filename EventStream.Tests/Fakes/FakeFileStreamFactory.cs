﻿namespace EventStream.Tests.Fakes
{
    using System.IO;
    using System.IO.Abstractions;

    public class FakeFileStreamFactory : IFileStreamFactory
    {
        private readonly IFileSystem fileSystem;

        public FakeFileStreamFactory(IFileSystem fileSystem)
        {
            this.fileSystem = fileSystem;
        }

        public Stream Create(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize, FileOptions options)
        {
            return this.fileSystem.File.Open(path, mode, access);
        }
    }
}
