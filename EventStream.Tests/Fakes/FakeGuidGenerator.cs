﻿namespace EventStream.Tests.Fakes
{
    using System.Collections.Generic;
    using System;

    public class FakeGuidGenerator : IGuidGenerator
    {
        private readonly Queue<Guid> nextGuids = new Queue<Guid>();

        public void SetNextGuids(params Guid[] nextGuids)
        {
            foreach (var guid in nextGuids)
                this.nextGuids.Enqueue(guid);
        }

        public Guid Generate()
        {
            if (this.nextGuids.Count == 0)
                return Guid.Empty;

            return this.nextGuids.Dequeue();
        }
    }
}
