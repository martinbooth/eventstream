﻿namespace EventStream.SampleClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    using EventStream.Client;

    using EventStream.Models;

    public class Program
    {
        public static void Main(string[] args)
        {
            var evt = new ManualResetEvent(false);

            var thread = new Thread(() =>
            {
                var lastGuid = Guid.Empty;

                var eventStreamHttpClient = new EventStreamHttpClient(new Uri("http://localhost:4120/events"));

                while (true)
                {
                    IEnumerable<Event> events = null;
                    
                    try
                    {
                        events = eventStreamHttpClient.ReadSinceAsync(lastGuid).Result;
                    }
                    catch (AggregateException ex)
                    {
                        Console.WriteLine(ex.InnerExceptions.First().Message);
                    }

                    if (events != null)
                    {
                        foreach (var @event in events)
                        {
                            ProcessEvent(@event);
                            lastGuid = @event.Id;
                        }
                    }

                    var signaled = evt.WaitOne(new TimeSpan(0, 0, 5));

                    if (signaled)
                        return;
                }
            });

            Console.WriteLine("Press any key to exit...");

            thread.Start();

            Console.ReadKey();

            evt.Set();
        }

        private static void ProcessEvent(Event @event)
        {
            Console.WriteLine("Id: {0}, Type: {1}, Timestamp: {2}, Body: {3}", @event.Id, @event.Type, @event.Timestamp, @event.Body);
        }
    }
}
