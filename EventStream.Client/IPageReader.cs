﻿namespace EventStream.Client
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public interface IPageReader
    {
        Task<Stream> Read(Uri uri);
    }
}
