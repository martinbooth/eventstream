﻿namespace EventStream.Client
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;

    internal class HttpPageReader : IPageReader
    {
        private readonly HttpClient httpClient;

        public HttpPageReader()
        {
            this.httpClient = new HttpClient();
        }

        public async Task<Stream> Read(Uri uri)
        {
            var response = await this.httpClient.GetAsync(uri);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStreamAsync();
        }
    }
}