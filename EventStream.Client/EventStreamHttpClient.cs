﻿namespace EventStream.Client
{
    using System;

    public class EventStreamHttpClient : EventStreamClient
    {
        public EventStreamHttpClient(Uri streamUri)
            : this(streamUri, new Configuration())
        {
        }

        public EventStreamHttpClient(Uri streamUri, Configuration configuration) 
            : base(streamUri, configuration, new HttpPageReader())
        {
        }
    }
}