﻿namespace EventStream.Client
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public class Configuration
    {
        private string extension;

        public Configuration()
        {
            this.JsonSerializerSettings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            this.Extension = "feed";
        }

        public JsonSerializerSettings JsonSerializerSettings { get; set; }

        public string Extension
        {
            get
            {
                return this.extension;
            }

            set
            {
                this.extension = value;
                this.NormalisedExtension = string.IsNullOrEmpty(value) ? string.Empty : (value.StartsWith(".") ? value : "." + value);
            }
        }

        internal string NormalisedExtension { get; private set; }
    }
}