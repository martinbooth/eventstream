namespace EventStream.Client
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    using EventStream.Models;

    public class EventStreamClient
    {
        private const string HeadFileNameBase = "head";

        private readonly Configuration configuration;

        private readonly IPageReader pageReader;

        private readonly Uri streamUri;

        internal EventStreamClient(Uri streamUri, Configuration configuration, IPageReader pageReader)
        {
            if (!streamUri.ToString().EndsWith("/"))
                streamUri = new Uri(streamUri + "/");

            this.streamUri = streamUri;
            this.configuration = configuration;
            this.pageReader = pageReader;
        }

        public Task<IEnumerable<Event>> ReadAllAsync()
        {
            return this.ReadSinceAsync(Guid.Empty);
        }

        public async Task<IEnumerable<Event>> ReadSinceAsync(Guid guid)
        {
            var headUri = new Uri(this.streamUri, HeadFileNameBase + this.configuration.NormalisedExtension);

            var headPageStream = await this.pageReader.Read(headUri);
            var reader = new StreamReader(headPageStream);

            try
            {
                var eventStack = new Stack<Event>();

                if (headPageStream == null)
                    return eventStack;

                var eventStreamHeader = EventStreamHeader.Read(reader);

                while (eventStreamHeader != null)
                {
                    var pageStack = new Stack<Event>();

                    while (!reader.EndOfStream)
                    {
                        Event @event;

                        try
                        {
                            @event = Event.Read(reader);
                        }
                        catch
                        {
                            break;
                        }

                        pageStack.Push(@event);
                    }

                    foreach (var @event in pageStack)
                    {
                        if (guid == @event.Id)
                            return eventStack;

                        eventStack.Push(@event);
                    }

                    if (string.IsNullOrWhiteSpace(eventStreamHeader.PreviousFilepath))
                        eventStreamHeader = null;
                    else
                    {
                        headPageStream.Dispose();
                        reader.Dispose();
                        headPageStream = await this.pageReader.Read(new Uri(this.streamUri, eventStreamHeader.PreviousFilepath));
                        reader = new StreamReader(headPageStream);
                        eventStreamHeader = EventStreamHeader.Read(reader);
                    }
                }

                return eventStack;
            }
            finally
            {
                if (headPageStream != null)
                    headPageStream.Dispose();

                reader.Dispose();
            }
        }
    }
}