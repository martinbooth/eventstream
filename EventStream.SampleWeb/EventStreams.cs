﻿namespace EventStream.SampleWeb
{
    using System.IO;
    using System.Web.Hosting;

    public static class EventStreams
    {
        static EventStreams()
        {
            var eventsPath = HostingEnvironment.MapPath("~/events");

            try
            {
                Directory.CreateDirectory(eventsPath);
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch
            {
                // probably already exists
            }

            Main = EventStream.Create(eventsPath, new Configuration { PageCount = 100 });
        }

        public static EventStream Main { get; private set; }
    }
}