﻿using System.Threading.Tasks;

namespace EventStream.SampleWeb
{
    using System;

    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            EventStreams.Main.RaiseEvent("app-start");
        }

        protected void Application_End(object sender, EventArgs e)
        {
            EventStreams.Main.RaiseEvent("app-end");
        }
    }
}