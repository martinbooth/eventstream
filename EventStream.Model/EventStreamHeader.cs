﻿namespace EventStream.Models
{
    using System;
    using System.IO;

    public class EventStreamHeader
    {
        public string PreviousFilepath { get; private set; }

        private readonly Header header;

        public EventStreamHeader(string previousFilepath)
        {
            this.PreviousFilepath = previousFilepath;
            this.header = new Header("Previous", previousFilepath);
        }

        public void Write(TextWriter tw)
        {
            this.header.Write(tw);
            tw.WriteLine();
        }

        public static EventStreamHeader Read(TextReader tr)
        {
            var header = Header.Read(tr);

            if (header == null)
                throw new Exception("Header not present");

            if (!header.Name.Equals("Previous", StringComparison.OrdinalIgnoreCase))
                throw new Exception("Expected previous header");

            var line = tr.ReadLine();

            if (line == null)
                throw new EndOfStreamException("Expected blank line to follow header");

            if (!string.IsNullOrWhiteSpace(line))
                throw new Exception("Expected blank line to follow header");

            return new EventStreamHeader(header.Value);
        }
    }
}
