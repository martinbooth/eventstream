﻿namespace EventStream.Models
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public class Event
    {
        private string type;

        public string Body { get; set; }

        public Guid Id { get; set; }

        public DateTime Timestamp { get; set; }

        public string Type
        {
            get
            {
                return this.type;
            }

            set
            {
                if (value.Contains("\n"))
                    throw new NotSupportedException();

                this.type = value;
            }
        }

        public static Event Read(TextReader tr)
        {
            var eventHeaders = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            var header = Header.Read(tr);

            while (header != null)
            {
                eventHeaders.Add(header.Name, header.Value);

                header = Header.Read(tr);
            }

            var @event = new Event
            {
                Id = Guid.Parse(eventHeaders["Id"]),
                Timestamp = DateTime.Parse(eventHeaders["Timestamp"]),
                Type = eventHeaders["Type"]
            };

            if (eventHeaders.ContainsKey("Content-Length"))
            {
                var byteCount = int.Parse(eventHeaders["Content-Length"]);

                var chars = new char[byteCount];

                tr.Read(chars, 0, byteCount);

                @event.Body = new string(chars);

                var line = tr.ReadLine();

                if (line == null || !string.IsNullOrWhiteSpace(line))
                    throw new Exception();

                line = tr.ReadLine();
                if (line == null || !string.IsNullOrWhiteSpace(line))
                    throw new Exception();
            }

            return @event;
        }

        public void Write(TextWriter tw)
        {
            foreach (var header in this.GetHeaders())
                header.Write(tw);

            if (this.Body != null)
            {
                tw.WriteLine();
                tw.WriteLine(this.Body);
            }

            tw.WriteLine();
        }

        private IEnumerable<Header> GetHeaders()
        {
            var eventHeaders = new List<Header>
            {
                new Header("Id", this.Id.ToString()),
                new Header("Type", this.Type),
                new Header("Timestamp", this.Timestamp.ToString("o")), 
            };

            if (this.Body != null)
                eventHeaders.Add(new Header("Content-Length", this.Body.Length.ToString()));

            return eventHeaders;
        }
    }
}