﻿namespace EventStream.Models
{
    using System;
    using System.IO;

    public class Header
    {
        public Header(string name, string value)
        {
            this.Name = name;
            this.Value = value;
        }

        public string Name { get; private set; }

        public string Value { get; private set; }

        public static Header Read(TextReader tr)
        {
            var line = tr.ReadLine();

            if (line == null)
                throw new EndOfStreamException();

            if (string.IsNullOrWhiteSpace(line))
                return null;

            var nameEnd = line.IndexOf(':');

            if (nameEnd == -1)
                throw new Exception();

            var name = line.Substring(0, nameEnd).Trim();
            var value = line.Substring(nameEnd + 1).Trim();

            return new Header(name, value);
        }

        public void Write(TextWriter tw)
        {
            tw.WriteLine(this.Name + ": " + this.Value);
        }
    }
}