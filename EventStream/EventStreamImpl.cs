﻿namespace EventStream
{
    using System;
    using System.IO;
    using System.IO.Abstractions;
    using System.Reflection;

    using global::EventStream.Models;

    using Newtonsoft.Json;

    internal class EventStreamImpl : EventStream
    {
        private const string HeadFileNameBase = "head";

        private readonly IFileStreamFactory fileStreamFactory;

        private readonly IFileSystem fileSystem;

        private readonly IGuidGenerator guidGenerator;

        private readonly ITimeProvider timeProvider;

        private readonly object writeSyncObject = new object();

        private Stream headPageStream;

        private int pageEventCount;

        public EventStreamImpl(IFileSystem fileSystem, IGuidGenerator guidGenerator, ITimeProvider timeProvider, IFileStreamFactory fileStreamFactory)
        {
            this.fileSystem = fileSystem;
            this.guidGenerator = guidGenerator;
            this.timeProvider = timeProvider;
            this.fileStreamFactory = fileStreamFactory;
        }

        public string HeadFileName
        {
            get
            {
                return HeadFileNameBase + this.Configuration.NormalisedExtension;
            }
        }

        public override void RaiseEvent(string type, object body)
        {
            if (type == null)
                throw new ArgumentNullException("type");

            if (string.IsNullOrWhiteSpace(type))
                throw new ArgumentException("Event type must be specified", "type");

            lock (this.writeSyncObject)
            {
                if (this.pageEventCount >= this.Configuration.PageCount)
                    this.ArchiveEventPage();

                var @event = new Event
                {
                    Id = this.guidGenerator.Generate(), 
                    Timestamp = this.timeProvider.Now, 
                    Type = type, 
                    Body = body == null ? null : JsonConvert.SerializeObject(body, this.Configuration.JsonSerializerSettings)
                };

                @event.Write(new StreamWriter(this.headPageStream) { AutoFlush = true });

                this.pageEventCount++;
            }
        }

        internal override void Initialise()
        {
            this.headPageStream = this.fileStreamFactory.Create(Path.Combine(this.StreamDirectory, this.HeadFileName), FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read, 4096, FileOptions.WriteThrough);
            this.CreateOrOpenStream();
        }

        private void ArchiveEventPage()
        {
            var archiveDirectory = Path.Combine(this.StreamDirectory, "archive");

            try
            {
                this.fileSystem.Directory.CreateDirectory(archiveDirectory);
            }
                
            // ReSharper disable once EmptyGeneralCatchClause
            catch
            {
                // already exists
            }

            var archiveFilename = this.GetRandomFilePathWithExtension(this.Configuration.NormalisedExtension);
            var archivePath = Path.Combine(archiveDirectory, archiveFilename);

            using (var archiveStream = this.fileStreamFactory.Create(archivePath, FileMode.CreateNew, FileAccess.Write, FileShare.None, 4096, FileOptions.WriteThrough))
            {
                this.headPageStream.Seek(0, SeekOrigin.Begin);

                this.headPageStream.CopyTo(archiveStream);
            }

            using (var ms = new MemoryStream())
            {
                new EventStreamHeader("archive/" + archiveFilename).Write(new StreamWriter(ms) { AutoFlush = true });

                this.headPageStream.Seek(0, SeekOrigin.Begin);
                ms.Seek(0, SeekOrigin.Begin);

                ms.CopyTo(this.headPageStream);

                this.headPageStream.SetLength(ms.Length);
            }

            this.pageEventCount = 0;
        }

        private void CreateOrOpenStream()
        {
            lock (this.writeSyncObject)
            {
                this.pageEventCount = 0;

                var sr = new StreamReader(this.headPageStream);
                if (this.headPageStream.Length == 0)
                {
                    var eventStreamHeader = new EventStreamHeader(null);

                    eventStreamHeader.Write(new StreamWriter(this.headPageStream)
                    {
                        AutoFlush = true
                    });
                }
                else if (!this.HasValidHeader(sr))
                {
                    throw new Exception("Header corrupted, cannot open stream");
                }
                else
                {
                    while (!sr.EndOfStream)
                    {
                        var currentPosition = this.GetActualPosition(sr);

                        try
                        {
                            Event.Read(sr);
                        }
                        catch
                        {
                            this.headPageStream.Seek(currentPosition, SeekOrigin.Begin);
                            this.headPageStream.SetLength(currentPosition);
                            this.headPageStream.Flush();
                            break;
                        }

                        this.pageEventCount++;
                    }
                }
            }
        }

        private string GetRandomFilePathWithExtension(string extension)
        {
            return this.guidGenerator.Generate() + extension;
        }

        private bool HasValidHeader(TextReader sr)
        {
            try
            {
                EventStreamHeader.Read(sr);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private long GetActualPosition(StreamReader reader)
        {
            var charBuffer = (char[])reader.GetType().InvokeMember("charBuffer", BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField, null, reader, null);

            var charPos = (int)reader.GetType().InvokeMember("charPos", BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField, null, reader, null);

            var numReadBytes = reader.CurrentEncoding.GetByteCount(charBuffer, 0, charPos);

            var byteLen = (int)reader.GetType().InvokeMember("byteLen", BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField, null, reader, null);

            return reader.BaseStream.Position - byteLen + numReadBytes;
        }
    }
}