namespace EventStream
{
    using System.IO;

    internal interface IFileStreamFactory
    {
        Stream Create(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize, FileOptions options);
    }
}