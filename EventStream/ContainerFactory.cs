﻿using System.IO;

namespace EventStream
{
    using System.IO.Abstractions;

    using Autofac;

    internal class ContainerFactory
    {
        static ContainerFactory()
        {
            CurrentContainer = new ContainerFactory().GetContainer();
        }

        public static IContainer CurrentContainer { get; set; }

        public IContainer GetContainer()
        {
            var containerBuilder = new ContainerBuilder();

            this.RegisterServices(containerBuilder);

            return containerBuilder.Build();
        }

        protected virtual void RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterType<FileSystem>().As<IFileSystem>().SingleInstance();
            builder.RegisterType<GuidGenerator>().As<IGuidGenerator>().SingleInstance();
            builder.RegisterType<TimeProvider>().As<ITimeProvider>().SingleInstance();
            builder.RegisterType<FileStreamFactory>().As<IFileStreamFactory>().SingleInstance();
            builder.RegisterType<EventStreamImpl>().As<EventStream>();
        }
    }
}