﻿namespace EventStream
{
    using System;

    internal interface ITimeProvider
    {
        DateTime Now { get; }
    }
}
