﻿namespace EventStream
{
    using Autofac;

    public abstract class EventStream
    {
        static EventStream()
        {
            DefaultConfiguration = new Configuration
            {
                PageCount = 100
            };
        }

        public static Configuration DefaultConfiguration { get; set; }

        public Configuration Configuration { get; set; }

        public string StreamDirectory { get; set; }

        public static EventStream Create(string path)
        {
            return Create(path, DefaultConfiguration);
        }

        public static EventStream Create(string path, Configuration configuration)
        {
            var eventStream = ContainerFactory.CurrentContainer.Resolve<EventStream>();
            eventStream.Configuration = configuration;
            eventStream.StreamDirectory = path;

            eventStream.Initialise();

            return eventStream;
        }

        public void RaiseEvent(string type)
        {
            this.RaiseEvent(type, null);
        }

        public abstract void RaiseEvent(string type, object body);

        internal abstract void Initialise();
    }
}