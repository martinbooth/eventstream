﻿namespace EventStream
{
    using System;

    internal class TimeProvider : ITimeProvider
    {
        public DateTime Now
        {
            get { return DateTime.UtcNow; }
        }
    }
}
