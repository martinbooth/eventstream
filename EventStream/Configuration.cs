﻿namespace EventStream
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public class Configuration
    {
        private string extension;

        public Configuration()
        {
            this.JsonSerializerSettings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            this.PageCount = 100;
            this.Extension = "feed";
        }

        public string Extension
        {
            get
            {
                return this.extension;
            }

            set
            {
                this.extension = value;
                this.NormalisedExtension = string.IsNullOrEmpty(value) ? string.Empty : (value.StartsWith(".") ? value : "." + value);
            }
        }

        public JsonSerializerSettings JsonSerializerSettings { get; set; }
        
        public int PageCount { get; set; }

        internal string NormalisedExtension { get; private set; }
    }
}