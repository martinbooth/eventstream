﻿namespace EventStream
{
    using System;

    internal interface IGuidGenerator
    {
        Guid Generate();
    }
}
