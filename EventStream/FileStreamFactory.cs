namespace EventStream
{
    using System.IO;

    internal class FileStreamFactory : IFileStreamFactory
    {
        public Stream Create(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize, FileOptions options)
        {
            return new FileStream(path, mode, access, share, bufferSize, options);
        }
    }
}