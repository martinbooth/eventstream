﻿namespace EventStream.Client.Tests
{
    using System;
    using System.IO.Abstractions.TestingHelpers;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class Reading_Events
    {
        protected internal MockFileSystem MockFileSystem { get; set; }

        [TestInitialize]
        public void BeforeEach()
        {
            this.MockFileSystem = new MockFileSystem()
                .WithTestStream();
        }

        [TestClass]
        public class When_Reading_All_Events : Reading_Events
        {
            [TestMethod]
            public async Task Returns_All_Events()
            {
                var client = new EventStreamClient(new Uri("c:\\stream"), new Configuration(), new FakePageReader(this.MockFileSystem));

                var newEvents = (await client.ReadAllAsync()).ToList();

                newEvents[0].Type = "event1";
                newEvents[0].Type = "event body1";

                newEvents[1].Type = "event2";
                newEvents[1].Type = "event body2";

                newEvents[2].Type = "event3";
                newEvents[2].Type = "event body3";
            }
        }

        [TestClass]
        public class When_Specifying_The_Previously_Read_Event : Reading_Events
        {
            [TestMethod]
            public async Task Only_Returns_Events_After_Specifed_Event()
            {
                var client = new EventStreamClient(new Uri("c:\\stream"), new Configuration(), new FakePageReader(this.MockFileSystem));

                var firstEvent = (await client.ReadAllAsync()).First();

                var newEvents = (await client.ReadSinceAsync(firstEvent.Id)).ToList();

                newEvents[0].Type = "event2";
                newEvents[0].Type = "event body2";

                newEvents[1].Type = "event3";
                newEvents[1].Type = "event body3";
            }
        }
    }
}
