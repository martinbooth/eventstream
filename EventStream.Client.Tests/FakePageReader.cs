﻿using System.Threading.Tasks;

namespace EventStream.Client.Tests
{
    using System;
    using System.IO;
    using System.IO.Abstractions.TestingHelpers;

    public class FakePageReader : IPageReader
    {
        private readonly MockFileSystem mockFileSystem;

        public FakePageReader(MockFileSystem mockFileSystem)
        {
            this.mockFileSystem = mockFileSystem;
        }

        public async Task<Stream> Read(Uri uri)
        {
            return this.mockFileSystem.File.OpenRead(uri.LocalPath);
        }
    }
}
