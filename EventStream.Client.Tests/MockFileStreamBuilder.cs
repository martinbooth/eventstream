﻿namespace EventStream.Client.Tests
{
    using System.IO;
    using System.IO.Abstractions;

    using System.IO.Abstractions.TestingHelpers;

    public static class MockFileStreamBuilder
    {
        public static MockFileSystem WithTestStream(this MockFileSystem fileSystem)
        {
            var eventStream = new EventStreamImpl(fileSystem, new GuidGenerator(), new TimeProvider(), new FakeFileStreamFactory(fileSystem))
            {
                Configuration = new global::EventStream.Configuration
                {
                    PageCount = 2
                },
                StreamDirectory = "C:\\stream"
            };

            eventStream.Initialise();

            eventStream.RaiseEvent("event1");
            eventStream.RaiseEvent("event2");
            eventStream.RaiseEvent("event3");

            return fileSystem;
        }

        private class FakeFileStreamFactory : IFileStreamFactory
        {
            private readonly IFileSystem fileSystem;

            public FakeFileStreamFactory(IFileSystem fileSystem)
            {
                this.fileSystem = fileSystem;
            }

            public Stream Create(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize, FileOptions options)
            {
                return this.fileSystem.File.Open(path, mode, access);
            }
        }
    }
}
