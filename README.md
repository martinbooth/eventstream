# EventStream

EventStream is a simple library to enable a producer to generate a stream of events and client to process this stream.

It is designed to be lightweight, human readable and reliable.

## Getting Started


###Producing events

You'll probably want to install the nuget package to produce events:

```
PM> Install-Package EventStream

```

You can have as many event streams as you like, but the same event stream cannot be opened more than once in the application.

Calls to RaiseEvent are thread safe.

The following code demonstrates the simplest example of raising an event in a web application. Note that you would typically want to keep a single instance of the EventStream around for the lifetime of the application

```cs
var eventsPath = Server.MapPath("~/events");

Directory.CreateDirectory(eventsPath);

var eventStream = EventStream.Create(eventsPath);

eventStream.RaiseEvent("testevent", new { message = "Success" });
```

In order to make these events accessible to another process you can host this in a webserver which only needs to allow access to the files generated. This is not a requirement however and any means of accessing these files can be supported (though you will need to implement an interface in the consumer to support anything other than HTTP)

Hint: you may need something like this in your web.config:

```xml
<system.webServer>
  <staticContent>
    <mimeMap fileExtension=".feed" mimeType="application/eventfeed" />
  </staticContent>
</system.webServer>
```

### Consuming events

You'll probably want to install the nuget package to consume events:

```
PM> Install-Package EventStream.Client

```

The following code will process any events not seen since last time

```cs
var eventStreamHttpClient = new EventStreamHttpClient(new Uri("http://localhost/events"));

var lastProcessedId = GetLastProcessedId();

foreach (var @event in (eventStreamHttpClient.ReadSinceAsync(Guid.Empty).Result))
{
    Process(@event);
    SetLastProcessedId(@event.Id);
}
```

GetLastProcessedId, SetLastProcessedId are simple functions that you will need to implement yourself in order to persist the Id of the event which was last processed using whatever storage mechanism you wish. (A simple file would be an easy option here)

### Notes about the event stream file format

Events are stored as a linked list of files with a configurable number of events per file. They are appended to the file at the front of the linked list until it has filled up at which point the file is moved to an archive folder and a new (empty) page is created at the front of the list (and this process repeats)

Here is an example of the format of the event stream files:

```
Previous: archive/f8ce5697-7094-45c7-a389-db167e25d488.feed

Id: d3ba45a7-b5c0-4298-a94c-c1cac75154fb
Type: testevent6244
Timestamp: 2015-02-23T06:15:43.9402308Z
Content-Length: 36

{"message":"hello from eventstream"}

Id: fe3b2dd3-376a-4ef3-8210-84d13fbc32f6
Type: testevent6245
Timestamp: 2015-02-23T06:15:43.9412308Z
Content-Length: 36

{"message":"hello from eventstream"}

```

One of the reasons against using a more common format (such as xml/atom, or json) is that this format easily allows new events to be appended to the file without having to parse, replace or rewrite the file. The file format hopefully looks similar enough to an http request to be easily understood by most people.

In addition, the Content-Length header allows a consumer to easily skip an event which does not need to process. This is far more effecient than parsing json to discover where the next event starts.

Finally, if the process writing the event terminates unexpectedly, mid-way through writing an event, it is easy to detect this when then process restarts and the event can be backed out (which EventStream does)