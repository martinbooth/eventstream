. .\build\psake_ext.ps1

properties {
  $base_dir = resolve-path .
  $sln = "$base_dir\EventStream.sln"
  $test_projects_base_dir = $base_dir
  $test_settings_file = $null
  $test_projects_dir = @('EventStream.Tests', 'EventStream.Client.Tests')
  $test_run_config = $null
  $configuration = "Release"
  $platform = "Any CPU"
  $overrideVSVersion = "12.0" # Set this to the version of Visual Studio you have (12.0 = 2013, 11.0 = 2012, 10.0 = 2010)
}

task ? -Description "Helper to display task info" {
	WriteDocumentation
}

task default -depends Test

task Clean -description "Deletes all compiled files from solution" { 
  exec { msbuild $sln /target:clean /p:Configuration=$configuration /p:Platform=$platform /p:VisualStudioVersion=$overrideVSVersion }
}

task Compile -description "Compiles all projects in solution" -depends Clean { 
  exec { msbuild $sln /p:Configuration=$configuration /p:Platform=$platform /p:VisualStudioVersion=$overrideVSVersion }
}

task Test -description 'Runs all CSharp unit tests in solution' -depends Compile {
    Run-CS-Tests $base_dir $test_projects_dir $test_projects_base_dir $configuration $null $test_settings_file $test_run_config $platform $overrideVSVersion
}

task Package -description "psake Package generates nuget package" -depends Test {
    Nuget-Package @('EventStream\EventStream.csproj', 'EventStream.Client\EventStream.Client.csproj') $configuration
}